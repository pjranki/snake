#ifndef _H_SNAKE_H_
#define _H_SNAKE_H_

#include <vector>
#include <functional>


class Snake
{
    public:
        typedef std::function<void(void)> DrawClearFn;
        typedef std::function<void(int16_t,int16_t)> DrawSquareFn;
        typedef std::function<int16_t(int16_t,int16_t)> RandomFn;

        Snake() : 
            draw_clear_(nullptr),
            draw_snake_(nullptr),
            draw_apple_(nullptr),
            random_(nullptr),
            w_(0), h_(0),
            xv_(0), yv_(0),
            px_(0), py_(0),
            ax_(0), ay_(0),
            tail_(5) {}
        ~Snake() {}
        void set_width(int16_t w) {
            w_ = w;
        }
        void set_height(int16_t h) {
            h_ = h;
        }
        void set_draw_clear(DrawClearFn func) {draw_clear_ = func;}
        void set_draw_snake(DrawSquareFn func) {draw_snake_ = func;}
        void set_draw_apple(DrawSquareFn func) {draw_apple_ = func;}
        void set_random(RandomFn func) {random_ = func;}
        void draw_clear() {
            if (draw_clear_) {
                draw_clear_();
            }
        }
        void draw_snake(int16_t x, int16_t y) {
            if (draw_snake_) {
                draw_snake_(x, y);
            }
        }
        void draw_apple(int16_t x, int16_t y) {
            if (draw_apple_) {
                draw_apple_(x, y);
            }
        }
        int16_t random(int16_t min, int16_t max) {
            if (random_) {
                return random_(min, max);
            }
            return min;
        }
        void reset() {
            xv_ = 0;
            yv_ = 0;
            px_ = w_ / 2;
            py_ = h_ / 2;
            trail_.clear();
            tail_ = 5;
            ax_ = random(0, w_ - 1);
            ay_ = random(0, h_ - 1);
        }
        void tick() {
            px_ += xv_;
            py_ += yv_;
            if (px_ < 0) {
                px_ = w_ - 1;
            }
            if (px_ > w_ - 1) {
                px_ = 0;
            }
            if (py_ < 0) {
                py_ = h_ - 1;
            }
            if (py_ > h_ - 1) {
                py_ = 0;
            }
            draw_clear();
            for (auto body : trail_) {
                draw_snake(body.x, body.y);
                if ((body.x == px_) && (body.y == py_)) {
                    tail_ = 5;
                }
            }
            trail_.push_back(Tail(px_, py_));
            while (trail_.size() > tail_) {
                trail_.erase(trail_.begin());
            }
            if ((ax_ == px_) && (ay_ == py_)) {
                tail_++;
                ax_ = random(0, w_ - 1);
                ay_ = random(0, h_ - 1);
            }
            draw_apple(ax_, ay_);
        }
        void key_left() {
            if (xv_ != 1) {
                xv_ = -1; yv_ = 0;
            }
        }
        void key_up() {
            if (yv_ != 1) {
                xv_ = 0; yv_ = -1;
            }
        }
        void key_right() {
            if (xv_ != -1) {
                xv_ = 1; yv_ = 0;
            }
        }
        void key_down() {
            if (yv_ != -1) {
                xv_ = 0; yv_ = 1;
            }
        }
    private:
        struct Tail {
            Tail() : x(0), y(0) {}
            Tail(int16_t x, int16_t y) : x(x), y(y) {}
            int16_t x;
            int16_t y;
        };
        DrawClearFn draw_clear_;
        DrawSquareFn draw_snake_;
        DrawSquareFn draw_apple_;
        RandomFn random_;
        std::vector<Tail> trail_;
        int16_t w_;
        int16_t h_;
        int16_t xv_;
        int16_t yv_;
        int16_t px_;
        int16_t py_;
        int16_t ax_;
        int16_t ay_;
        int16_t tail_;
};

#endif
