Snake
=====
Platform independent Snake.
Based off YouTube video https://www.youtube.com/watch?v=xGmXxpIj6vs

Language
--------
C++11 and up (should compile for anything, doesn't use any OS APIs).

Arduino
-------
Checkout/copy this library to your Arduino libraries folder.

How to use it
-------------
Include the library:

```cpp
#include <snake.h>
```

Construct the class:

```cpp
Snake snake;
```

Set the grid size for the game, 20 x 20 seems to be the norm:

```cpp
snake.set_width(20); 
snake.set_height(20);
```

Tell the snake game how to do things:

```cpp
snake.set_draw_clear([]() {
    // Clear grid of everything
});
snake.set_draw_snake([](int16_t x, int16_t y) {
    // Draw piece (block) of the snake at (x, y)
});
snake.set_draw_apple([](int16_t x, int16_t y) {
    // Draw the apple at (x, y)
});
snake.set_random([](int16_t min, int16_t max) -> int16_t {
    // return a random number between min and max (inclusive)
});
```

During the game, call the following functions when you get user input:

```cpp
snake.key_left();
snake.key_up();
snake.key_right();
snake.key_down();
```

Call the tick function 15 frames per second (or every 1000/15 milliseconds):

```cpp
snake.tick();
```

To reset the game at any point, call the reset function:

```cpp
snake.reset();
```
